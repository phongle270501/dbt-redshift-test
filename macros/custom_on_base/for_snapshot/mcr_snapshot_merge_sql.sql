{% macro snapshot_merge_sql(target, source, insert_cols) -%}
    {%- set insert_cols_csv = insert_cols | join(', ') -%}
    {%- set tempList = [] %}
    {%- for col in insert_cols %}
        {{- tempList.append('DBT_INTERNAL_SOURCE.'+ col ) or "" -}}
    {%- endfor -%}
    {%- set insert_cols_csv_src = tempList | join(', ') -%}

    update {{target}}
    set dbt_valid_to = DBT_INTERNAL_SOURCE.dbt_valid_to
    from {{ source }} as DBT_INTERNAL_SOURCE
    where DBT_INTERNAL_SOURCE.dbt_scd_id = {{target}}.dbt_scd_id
      and DBT_INTERNAL_SOURCE.dbt_change_type in ('update', 'delete')
      --and {{target}}.dbt_valid_to is null
      and {{target}}.dbt_valid_to = cast('{{ var("end_date") }}' as timestamp)
    ;

    insert into {{target}} ({{ insert_cols_csv }})
    select {{insert_cols_csv_src}}
    from {{ source }} as DBT_INTERNAL_SOURCE
    where DBT_INTERNAL_SOURCE.dbt_change_type = 'insert';

{% endmacro %}

