{% macro generate_alias_name(custom_alias_name=none, node=none) -%}

    {%- if custom_alias_name -%}
        {{log('11111' ~ custom_alias_name,info=true)}}
        {{ custom_alias_name | trim }}

    {%- elif node.version -%}
        {{log('22222',info=true)}}
        {{ return(node.name ~ "_v" ~ (node.version | replace(".", "_"))) }}

    {%- else -%}
        {{log('3333',info=true)}}
        {{ node.name }}

    {%- endif -%}

{%- endmacro %}