{% materialization scd, adapter='redshift' %}
    {#-- get config #}
    {%- set target_relation = this %}
    {%- set unique_key = config.get('unique_key') %}
    {%- set excludes_column_name = config.get('identity_col', none) %}
    {#-- +++++ strategy : accepted values: type0 or type2, default type2 #}
    {%- set strategy = config.get('strategy', none) %} 

    {%- set type1_col = config.get('type1_col', none) %}

    {%- set tmp_identifier = "temp_" ~ target_relation.identifier %}
    {%- set tmp_relation = target_relation.incorporate(path={"identifier": tmp_identifier}) -%}
    {%- set existing_relation = load_relation(this) -%}
    {% if existing_relation is none or should_full_refresh() %}
        {%- set build_sql =   build_dim_initial_sql(target_relation, tmp_relation) %}
    {% else %}
        {%- set build_sql = build_dim_sql(target_relation, tmp_relation, unique_key, excludes_column_name, strategy, type1_col) %}
    {% endif %}
{{- run_hooks(pre_hooks) -}}
    {%- call statement('main') -%}
        {{ build_sql }}
    {% endcall %}
    {{ run_hooks(post_hooks) }}
    {% do adapter.commit() %}
    {% set target_relation = this.incorporate(type='table') %}
    {% do persist_docs(target_relation, model) %}
    {{ return({'relations': [target_relation]}) }}
{% endmaterialization %}



{%- macro build_dim_initial_sql(target_relation, temp_relation) -%}
    {{ create_table_as(True, temp_relation, sql) }}
    {%- set initial_sql -%}
        SELECT
          * 
        FROM
          {{ temp_relation }}
    {%- endset -%}
    {{ create_table_as(False, target_relation, initial_sql) }}
{%- endmacro -%}


{%- macro build_dim_sql(target_relation, temp_relation, unique_key, excludes_column_name, strategy, type1_col=[]) -%}
    {%- set columns = adapter.get_columns_in_relation(target_relation) -%}
    {#-- LyTTT: loai bo cot danh identity --#}
    {% set csv_colums = get_quoted_csv_fss(columns | map(attribute="name"), excludes_column_name) %}
    {% set csv_colums_src_as = get_quoted_csv_fss_v2(columns | map(attribute="name"), 's', excludes_column_name) %}
    {% set csv_colums_trg_as = get_quoted_csv_fss_v2(columns | map(attribute="name"), 't', excludes_column_name) %}
    {% set bsn_col_names = mcr_get_bussiness_columns_v2(this, 't', excludes_column_name) %}

{# nam.do add type 3 #}
    {% set list_type2_col_src = mcr_get_bussiness_columns_v3(this, 's', excludes_column_name, type1_col, unique_key) %}
    {% set list_type2_col_trg = mcr_get_bussiness_columns_v3(this, 't', excludes_column_name, type1_col, unique_key) %}

{# end - nam.do add type 3 #}

    {# {{log('bsn_col_names: '~ bsn_col_names, info =true)}} #}

    {{ create_table_as(True, temp_relation, sql) }}

    {% if strategy == 'type0' %}
        {{log('scd type 0', info = true)}}
        {% set ns = namespace(strJoin = '') %}
        {% for col in unique_key %}
            {% set ns.strJoin = ns.strJoin ~ target_relation ~'.'~  col  ~ '=' ~ 'tmp.'  ~col  %}
            {%- if not loop.last %} 
                {% set ns.strJoin = ns.strJoin ~' and ' %}
            {% endif -%}            
        {% endfor %}         
        INSERT INTO {{ target_relation }} ({{ csv_colums }})
        SELECT DISTINCT
        {{ csv_colums }}
        FROM
        {{ temp_relation.table }} tmp
        {#-- LyTTT: Them dieu kien NOT EXISTS theo unique_key #}
        WHERE NOT EXISTS( select 1 from {{ target_relation }} 
                        where {{ns.strJoin}} 
                        );
    {% elif strategy == 'type1' %}

        {{log('scd type 1', info = true)}}
        {% set ns = namespace(strJoin = '') %}
        {% for col in unique_key %}
            {% set ns.strJoin = ns.strJoin ~ target_relation ~'.'~  col  ~ '=' ~ 'tmp.'  ~col  %}
            {%- if not loop.last %} 
                {% set ns.strJoin = ns.strJoin ~' and ' %}
            {% endif -%}            
        {% endfor %}   

        {% set ns1 = namespace(strJoin = '') %}
        {% for col in unique_key %}
            {% set ns1.strJoin = ns1.strJoin  ~'s.'~  col  ~ '=' ~ 't.'  ~col  %}
            {%- if not loop.last %} 
                {% set ns1.strJoin = ns1.strJoin ~' and ' %}
            {% endif -%}            
        {% endfor %} 

        {% set ns2 = namespace(strDel = '') %}
        {% for col in unique_key %}
            {% set ns2.strDel = ns2.strDel  ~'s.'~  col  ~ ' is null '  %}
            {%- if not loop.last %} 
                {% set ns2.strDel = ns2.strDel ~' and ' %}
            {% endif -%}            
        {% endfor %}
        
        {% set ns3 = namespace(strUpd = '') %}
        {% for col in unique_key %}
            {% set ns3.strUpd = ns3.strUpd  ~  col  ~ '=' ~ 'tmp.'  ~col  %}
            {%- if not loop.last %} 
                {% set ns3.strUpd = ns3.strUpd ~' , ' %}
            {% endif -%}            
        {% endfor %}      

        {% if unique_key is not none %}
            {%- set tmp_bns_identifier = "temp_bns_" ~ target_relation.identifier %}
            create temporary table {{tmp_bns_identifier}} as 
            select {{csv_colums_src_as}}, 'I' flg
            from {{ temp_relation.table }}  s
            where not exists (select 1 from {{ target_relation }} t where {{ns1.strJoin}} )
            
            union all
            
            select {{csv_colums_src_as}}, 'U' flg
            from {{ temp_relation.table }}  s
            inner join {{ target_relation }} t 
            on {{ns1.strJoin}}
            where 1=1  
            and nvl(s.compare_key, '$') != nvl({{ dbt_utils.generate_surrogate_key(bsn_col_names) }}, '$')
            
            union all 

            select {{csv_colums_trg_as}}, 'D' flg
            from {{ target_relation }} t 
            left join {{ temp_relation.table }}  s 
            on {{ns1.strJoin}} 
            where 1=1
            and {{ns2.strDel}} 
            ;

            update {{ target_relation }}
            set tf_deleted_at = '{{ var("etl_date") }}' :: date
            from {{tmp_bns_identifier}}  tmp
            where {{ns.strJoin}} and tmp.flg = 'D'
            ;

            update {{ target_relation }}
            set {{ns3.strUpd}}
            from {{tmp_bns_identifier}}  tmp
            where {{ns.strJoin}} and tmp.flg = 'U'
            ;

            insert into {{ target_relation }} ({{ csv_colums }})
            (
                select {{ csv_colums }}
                from {{tmp_bns_identifier}} tmp
                where tmp.flg = 'I'

            );
        {% endif %}
    {% elif strategy == 'type2'  %} 
        
        {{log('scd type 2', info = true)}}
        {% set ns = namespace(strJoin = '') %}
        {% for col in unique_key %}
            {% set ns.strJoin = ns.strJoin ~ target_relation ~'.'~  col  ~ '=' ~ 'tmp.'  ~col  %}
            {%- if not loop.last %} 
                {% set ns.strJoin = ns.strJoin ~' and ' %}
            {% endif -%}            
        {% endfor %}   

        {% set ns1 = namespace(strJoin = '') %}
        {% for col in unique_key %}
            {% set ns1.strJoin = ns1.strJoin  ~'s.'~  col  ~ '=' ~ 't.'  ~col  %}
            {%- if not loop.last %} 
                {% set ns1.strJoin = ns1.strJoin ~' and ' %}
            {% endif -%}            
        {% endfor %} 

        {% set ns2 = namespace(strDel = '') %}
        {% for col in unique_key %}
            {% set ns2.strDel = ns2.strDel  ~'s.'~  col  ~ ' is null '  %}
            {%- if not loop.last %} 
                {% set ns2.strDel = ns2.strDel ~' and ' %}
            {% endif -%}            
        {% endfor %}         

        {% if unique_key is not none %}
            {%- set tmp_bns_identifier = "temp_bns_" ~ target_relation.identifier %}
            create temporary table {{tmp_bns_identifier}} as 
            select {{csv_colums_src_as}}, 'U' flg
            from {{ temp_relation.table }}  s
            left join {{ target_relation }} t 
            on {{ns1.strJoin}} 
            and t.tf_updated_at = '{{ var("end_date") }}' :: date
            where 1=1  
            and nvl(s.compare_key, '$') != nvl({{ dbt_utils.generate_surrogate_key(bsn_col_names) }}, '$')
            union all 
            SELECT {{csv_colums_trg_as}}, 'D' flg
            FROM {{ target_relation }} t 
            LEFT JOIN {{ temp_relation.table }}  s 
            on {{ns1.strJoin}} 
            WHERE 1=1
            and t.tf_updated_at = '{{ var("end_date") }}' :: date
            and {{ns2.strDel}} 
            ;

            update {{ target_relation }}
            set tf_updated_at = '{{ var("etl_date") }}' :: date
            from {{tmp_bns_identifier}}  tmp
            where {{ns.strJoin}}
            and {{ target_relation }}.tf_updated_at = '{{ var("end_date") }}' :: date
            ;

            insert into {{ target_relation }} ({{ csv_colums }})
            (
                select {{ csv_colums }}
                from {{tmp_bns_identifier}} tmp
                where tmp.flg = 'U'

            );
        {% endif %}

    {# nam.do add type 3 #}
    
    {% elif strategy == 'type3' %}

    {{log('scd type 3', info = true)}}


    {% set unique_key_col_equal = namespace(strJoin = '') %}
        {% for col in unique_key %}
            {% set unique_key_col_equal.strJoin = unique_key_col_equal.strJoin ~ 's.' ~  col  ~ '=' ~ 't.' ~ col  %}
            {%- if not loop.last %} 
                {% set unique_key_col_equal.strJoin = unique_key_col_equal.strJoin ~' and ' %}
            {% endif -%}
        {% endfor %}
    {# {{log('unique_key_col_equal statement: ' ~ unique_key_col_equal.strJoin, info = true)}} #}

    {% set list_type1_col_src = [] %}
        {% for col in type1_col %}
            {{ list_type1_col_src.append('s.' ~ col) or "" }}
        {% endfor %}

    {% set list_type1_col_trg = [] %}
        {% for col in type1_col %}
            {{ list_type1_col_trg.append('t.' ~ col) or "" }}
        {% endfor %}

    {# comment type 1 type1_cols_upd #}
        {% set type1_cols_upd = namespace(strJoin = '') %}
        {% for col in type1_col %}
            {% set type1_cols_upd.strJoin = type1_cols_upd.strJoin ~ col  ~ '=' ~ 's.' ~ col  %}
            {%- if not loop.last %} 
                {% set type1_cols_upd.strJoin = type1_cols_upd.strJoin ~' , ' %}
            {% endif -%}     
        {% endfor %}

            {% set unique_key_col_equal_2 = namespace(strJoin = '') %}
        {% for col in unique_key %}
            {% set unique_key_col_equal_2.strJoin = unique_key_col_equal_2.strJoin ~ target_relation ~'.'~col  ~ '=' ~ 's.' ~ col  %}
            {%- if not loop.last %} 
                {% set unique_key_col_equal_2.strJoin = unique_key_col_equal_2.strJoin ~' and ' %}
            {% endif -%}
        {% endfor %}

    {% if unique_key is not none %}
            {%- set tmp_bns_identifier = "temp_bns_" ~ target_relation.identifier %}
            create temporary table {{tmp_bns_identifier}} as 
            select {{csv_colums_src_as}}, 'I' flg
            from {{ temp_relation.table }}  s
            where not exists (select 1 from {{ target_relation }} t where {{unique_key_col_equal.strJoin}}
            and nvl({{ dbt_utils.generate_surrogate_key(list_type2_col_src) }}, '$') = nvl({{ dbt_utils.generate_surrogate_key(list_type2_col_trg) }}, '$')
            and t.tf_updated_at = '{{ var("end_date") }}' :: date)
            
            union all
            
            select {{csv_colums_src_as}}, 'U' flg
            from {{ temp_relation.table }}  s
            inner join {{ target_relation }} t 
            on {{unique_key_col_equal.strJoin}}
            and nvl({{ dbt_utils.generate_surrogate_key(list_type2_col_src) }}, '$') = nvl({{ dbt_utils.generate_surrogate_key(list_type2_col_trg) }}, '$')
            and t.tf_updated_at = '{{ var("end_date") }}' :: date
            where 1=1
            and nvl({{ dbt_utils.generate_surrogate_key(list_type1_col_trg) }}, '$') != nvl({{ dbt_utils.generate_surrogate_key(list_type1_col_src) }}, '$')
            
            union all 

            select {{csv_colums_trg_as}}, 'D' flg
            from {{ target_relation }} t
            where 1=1
            and t.tf_updated_at = '{{ var("end_date") }}' :: date
            and not exists (select 1 from {{ temp_relation.table }}  s where {{unique_key_col_equal.strJoin}}
            and nvl({{ dbt_utils.generate_surrogate_key(list_type2_col_src) }}, '$') = nvl({{ dbt_utils.generate_surrogate_key(list_type2_col_trg) }}, '$')
            )
            ;

        {# close dim bi xoa #}
            update {{ target_relation }}
            set tf_updated_at = '{{ var("etl_date") }}' :: date
            from {{tmp_bns_identifier}} s
            where {{ unique_key_col_equal_2.strJoin }}
            and s.flg = 'D'
            ;

        {# update truong type 1 #}
            update {{ target_relation }}
            set {{type1_cols_upd.strJoin}}
            from {{tmp_bns_identifier}} s
            where {{unique_key_col_equal_2.strJoin}}
            and {{ target_relation }}.tf_updated_at = '{{ var("end_date") }}' :: date
            and s.flg = 'U'
            ;

        {# insert new = dim moi + dim thay doi type 2 #}
            insert into {{ target_relation }} ({{ csv_colums }})
            (
                select {{ csv_colums }}
                from {{tmp_bns_identifier}} tmp
                where tmp.flg = 'I' 

            );
        {% endif %}

    {# end - nam.do add type 3 #}

    {% endif %}
{%- endmacro -%}

{%- macro get_quoted_csv_fss(column_names, excludes_column_name) -%}
    {%- set excludes_column_name = '' if excludes_column_name is none else excludes_column_name %}
    {% set quoted = [] %}
    {% for col in column_names -%}
        {%- if col|upper != excludes_column_name|upper -%}
            {%- do quoted.append(adapter.quote(col)) -%}
        {%- endif -%}
    {%- endfor %}

    {%- set dest_cols_csv = quoted | join(', ') -%}
    {{ return(dest_cols_csv) }}
{%- endmacro -%}


{%- macro get_quoted_csv_fss_v2(column_names, alias, excludes_column_name) -%}
    {%- set excludes_column_name = '' if excludes_column_name is none else excludes_column_name %}
    {% set quoted = [] %}
    {% for col in column_names -%}
        {%- if col|upper != excludes_column_name|upper -%}
            {%- do quoted.append(alias ~'.'~adapter.quote(col)) -%}
        {%- endif -%}
    {%- endfor %}

    {%- set dest_cols_csv = quoted | join(', ') -%}
    {{ return(dest_cols_csv) }}
{%- endmacro -%}