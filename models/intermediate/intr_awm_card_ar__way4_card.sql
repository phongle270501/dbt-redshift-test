{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}}',
        tags = ['awm', 'card_ar', 'intr_awm_card_ar__way4_card']
    )
}}


with

way4_acnt_contract as (

    select   
            a.*
    from {{ ref('stg_way4__acnt_contract') }} a

),

way4_f_i as (

    select
            id, 
            case 
                when bank_code = '0100' then 'VN0010001'
            else 'VN001'||bank_code
            end bank_code
    from {{ ref('stg_way4__f_i') }}
),

final as (
    select
        {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ID'}, {'text': 'WAY4_ACNT_CONTRACT'}]) }} as CARD_AR_ID,
        acnt_ctr.ID unq_id_in_src_stm,
        {{ mcr_generate_surrogate_key([{'text': 'W4-ACC-CTR-ST'},{'column':'acnt_ctr.CONTR_STATUS' }, {'text': 'CSV_CV'}]) }} as card_lcs_tp_id,
        acnt_ctr.DATE_EXPIRE::date as end_dt,
        acnt_ctr.DATE_OPEN::date as eff_dt,
        case 
            when acnt_ctr.CON_CAT = 'A' AND acnt_ctr.PCAT = 'M' then {{ mcr_generate_surrogate_key([{'text': 'AR_TP'},{'text': 'W4-DVC-MRCH-CTR'}, {'text': 'CSV_CV'}]) }}
            when acnt_ctr.CON_CAT = 'A' AND acnt_ctr.PCAT = 'C' then {{ mcr_generate_surrogate_key([{'text': 'AR_TP'},{'text': 'W4-ACC-CTR'}, {'text': 'CSV_CV'}]) }}
            when acnt_ctr.CON_CAT = 'C' then  {{ mcr_generate_surrogate_key([{'text': 'AR_TP'},{'text': 'W4-CARD-CTR'}, {'text': 'CSV_CV'} ]) }} 
        else 
            {{ mcr_generate_surrogate_key([{'text': 'AR_TP'},{'text': 'W4-OTHER-CTR'}, {'text': 'CSV_CV'} ]) }} end AR_TP_ID,
        case
            when acnt_ctr.ACNT_CONTRACT__OID is null then null
            else {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ACNT_CONTRACT__OID'}, {'text': 'WAY4_ACNT_CONTRACT'}]) }} 
        end as prn_ar_id,
        {{ mcr_generate_surrogate_key([{'column': 'fi.bank_code'}, {'text': 'T24_VPB_COMPANY'} ]) }} opn_br_id,
        REGEXP_SUBSTR(acnt_ctr.ADD_INFO_04,'(AL(\d)+)', 1, 1) as dsc,
        {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.CLIENT__ID'}, {'text': 'WAY4_CLIENT'} ]) }} cst_id,
        acnt_ctr.CONTRACT_NUMBER::varchar as card_ar_nm,  
        {{ mcr_generate_surrogate_key([{'text': 'WAY4_ACNT_CONTRACT'} , {'text': 'CSV_SRC_STM'}]) }} as src_stm_id,
        '{{ model.path }}' job_nm,        
        '{{ var("etl_date") }}'::date as tf_created_at
    from way4_acnt_contract acnt_ctr
    left join way4_f_i fi on acnt_ctr.F_I = fi.ID
    where Acnt_ctr.Base_relation is null

)
select * from final


