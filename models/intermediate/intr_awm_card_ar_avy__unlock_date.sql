{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}} ',
        tags = ['awm','card_ar_avy', 'intr_awm_card_ar_avy__unlock_date']
    )
}}


with

acnt_ctr as (

    select * from {{ ref('stg_way4__tbl_w4_acnt_contract') }}

),

final as (
    select 
        {{ mcr_generate_surrogate_key([{'column': 'a.ID'}, {'text': 'WAY4_ACNT_CONTRACT'}]) }} as CARD_AR_ID,
        {{ mcr_generate_surrogate_key([{'column': 'a.ID'}, {'text': 'W4-CARD-CTR-UNLOCK-DT'}, {'text':'WAY4_ACNT_CONTRACT'}]) }} as CARD_AR_AVY_ID,
        a.UNLOCK_DATE ACT_STRT_DT,
        {{ mcr_generate_surrogate_key([{'text': 'AR-AVY-TP'}, {'text': 'W4-CARD-CTR-UNLOCK-DT'} , {'text': 'CSV_CV'}]) }} as AVY_TP_ID,
        '{{ var("etl_date") }}'::date as TF_CREATED_AT
    from acnt_ctr a
    where a.CON_CAT = 'C'
    and a.UNLOCK_DATE is not null

)

select * from final






