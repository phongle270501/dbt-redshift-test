{{
    config(
        materialized='table',
        pre_hook = 'truncate table {{this}}',
        tags = ["intr_awm_ip", "ip"]
    )
}}


with

way4_client as (

    select * from {{ ref('stg_way4__client') }}    

),


final as (
    select
    {{ mcr_generate_surrogate_key([{'column': 'cst.id'} , {'text': 'WAY4_CLIENT'}]) }} as ip_id,
    cst.short_name::varchar as ip_nm,
    cst.id::varchar as UNQ_ID_IN_SRC_STM,
    {{ mcr_generate_surrogate_key([{'text': 'WAY4_CLIENT'} , {'text': 'CSV_SRC_STM'}]) }} as SRC_STM_ID,
    '{{ var("etl_date") }}'::date as TF_CREATED_AT
    from way4_client cst
)

select * from final

