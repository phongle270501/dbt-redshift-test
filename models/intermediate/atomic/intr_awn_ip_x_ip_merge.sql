{#
    job name : intr_awn_ip_x_ip_merge
    mapping ID : EAD_RA IP_X_IP Customer Merge
    created by : phong.le
    modified by | date modify | description
    phong.le    | 2023-01-15  | update code
#}

{{
    config(
        materialized='incremental',
        pre_hook = "truncate table {{this}}",
        tags = ["intr_awm_ip_x_ip", "ip_x_ip"]
    )
}}

with WAY4_CLIENT as (
    select 
        id,
        case 
            when REG_NUMBER LIKE '999%' AND CCAT = 'C' THEN SUBSTRING(REG_NUMBER, 4)
            else REG_NUMBER 
        end as REG_NUMBER,
        client_number
    from {{ref("stg_way4__client")}}
    where AMND_STATE = 'A'
),

T24_CUSTOMER as (
    select 
        *
    from {{ref("stg_t24__customer")}}
),

T24_CUSTOMER_DETAILS as (
    select 
        * 
    from {{ref("stg_t24__customer_details")}}
    where name_1 not like '%KHONG%SU%DUNG%' 
      and name_1 not like '%Ko%SU%DUNG%'
      and name_1 not like '%K SU DUNG%'
      and name_1 not like '%Khong Sd%'
    and M = '1' and S = '1'
),

union_all as (
    select 
        w4_cst.id,
        t24_cst_dtl.CUSTOMER_CODE,
        '1' priority
    from WAY4_CLIENT w4_cst
    join T24_CUSTOMER_DETAILS t24_cst_dtl on w4_cst.REG_NUMBER = t24_cst_dtl.LEGAL_ID
    union all
    select 
        w4_cst.id,
        t24_cst_dtl.CUSTOMER_CODE,
        '2' priority
    from WAY4_CLIENT w4_cst
    join T24_CUSTOMER_DETAILS t24_cst_dtl on w4_cst.REG_NUMBER = t24_cst_dtl.DOC_NUM
    union all 
    select  
        w4_cst.id,
        t24_cst.CUSTOMER_CODE,
        '3' priority
    from WAY4_CLIENT w4_cst
    join T24_CUSTOMER t24_cst on w4_cst.CLIENT_NUMBER = t24_cst.CUSTOMER_CODE
),

final as (
    select 
        {{mcr_generate_surrogate_key([{'column': 'f.id'},{'text':'WAY4_CLIENT'}])}} as SBJ_IP_ID,
        {{mcr_generate_surrogate_key([{'text': 'IP-X-IP-TP'},{'text':'CST-TO-T24CIF'},{'text':'CSV_CV'}])}} as IP_X_IP_TP_ID,
        {{mcr_generate_surrogate_key([{'column': 'f.CUSTOMER_CODE'},{'text':'T24_CUSTOMER'}])}} as OBJ_IP_ID,
        {{mcr_generate_surrogate_key([{'text': 'WAY4_CLIENT'},{'text':'CSV_SRC_STM'}])}} as SRC_STM_ID,
        '{{ var("etl_date") }}'::date as TF_CREATED_AT,
        row_number() over (partition by f.ID order by f.priority asc, f.customer_code asc) row_number
    from union_all f
)

select SBJ_IP_ID, IP_X_IP_TP_ID, OBJ_IP_ID, SRC_STM_ID, TF_CREATED_AT from final where row_number = 1