{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}}',
        tags = ["intr_awm_ip", "ip"]
    )
}}



with
t24_customer as (
    select * from {{ ref('stg_t24__customer') }} 
),

t24_customer_details as (
    select * from {{ ref('stg_t24__customer_details') }}
),

final as (
    select
    {{ mcr_generate_surrogate_key([{'column': 'cst.customer_code'} , {'text': 'T24_CUSTOMER'}]) }} as ip_id,
    cst_dtl.NAME_1::varchar as ip_nm,
    cst.CUSTOMER_CODE::varchar as UNQ_ID_IN_SRC_STM,
    {{ mcr_generate_surrogate_key([{'text': 'T24_CUSTOMER'} , {'text': 'CSV_SRC_STM'}]) }} as SRC_STM_ID,
    '{{ var("etl_date") }}'::date as TF_CREATED_AT
    from t24_customer cst
    join t24_customer_details cst_dtl on cst.customer_code = cst_dtl.customer_code
    where cst_dtl.M = '1' and cst_dtl.S = '1'
)
select * from final

