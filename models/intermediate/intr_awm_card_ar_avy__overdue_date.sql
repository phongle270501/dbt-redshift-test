{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}} ',
        tags = ['awm','card_ar_avy', 'intr_awm_card_ar_avy__overdue_date']
    )
}}


with

acnt_ctr as (

    select * from {{ ref('stg_way4__acnt_contract') }}

),

pd_date as 
(
    select * from {{ ref('stg_way4__vpb_acnt_pd_date')}}

),

final as (
    select 
        {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ID'}, {'text': 'WAY4_ACNT_CONTRACT'}]) }} as CARD_AR_ID,
        {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ID'}, {'text': 'W4-ACC-CTR-OUDE-DT'}, {'text':'WAY4_ACNT_CONTRACT'}]) }} as CARD_AR_AVY_ID,
        pd_date.PD_DATE ACT_STRT_DT,
        {{ mcr_generate_surrogate_key([{'text': 'AR-AVY-TP'}, {'text': 'W4-ACC-CTR-ODUE-DT'} , {'text': 'CSV_CV'}]) }} as AVY_TP_ID,
        '{{ var("etl_date") }}'::date as TF_CREATED_AT
    from acnt_ctr
    Join pd_date on acnt_ctr.ID = pd_date.ACNT_CONTRACT__OID
    where acnt_ctr.CON_CAT = 'C'
 
)

select * from final