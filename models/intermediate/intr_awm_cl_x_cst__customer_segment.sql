{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}} '
    )
}}


with

t24_cst as (

    select * from {{ ref('stg_t24__customer') }}

),

final as (
    select 
        {{ mcr_generate_surrogate_key([{'column': 'a.CUSTOMER_CODE'}, {'text': 'T24_CUSTOMER'}]) }} as CST_ID,
        {{ mcr_generate_surrogate_key([{'text': 'CL-X-IP-TP'}, {'text': 'T24-CST-TO-SEG'}, {'text': 'CSV_CV'}]) }} as CL_X_CST_TP_ID,
        {{ mcr_generate_surrogate_key([{'text': 'T24-191'}, {'text': 'CSV_CL_SCM'}]) }} as CL_SCM_ID,
        {{ mcr_generate_surrogate_key([{'text': 'T24-191'}, {'column': 'a.SEGMENT'}, {'text': 'CSV_CV'} ]) }} as CL_ID,
        {{ mcr_generate_surrogate_key([{'text': 'T24_CUSTOMER'}, {'text': 'CSV_SRC_STM'}]) }} as SRC_STM_ID,
        '{{ var("etl_date") }}'::date as TF_CREATED_AT 
    from t24_cst a
    where a.SEGMENT is not null
)

select * from final



