{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}}',
        tags = ["intr_awm_ip", "ip"]
    )
}}


with

t24_vpb_company as (

    select * from {{ ref('stg_t24__vpb_company') }}

),

t24_vpb_company_details as (

    select * from {{ ref('stg_t24__vpb_company_details') }}

),

final as (
    select
    {{ mcr_generate_surrogate_key([{'column': 'br.id'}, {'text': 'T24_VPB_COMPANY'}]) }} as ip_id,
    br_dtl.BRANCH_NAME::varchar as ip_nm,
    br.ID::varchar as UNQ_ID_IN_SRC_STM,
    {{ mcr_generate_surrogate_key([{'text': 'T24_VPB_COMPANY'} , {'text': 'CSV_SRC_STM'}]) }} as SRC_STM_ID,
    '{{ var("etl_date") }}'::date as TF_CREATED_AT
    from t24_vpb_company br
    left join t24_vpb_company_details br_dtl on br.ID = br_dtl.ID
    where br_dtl.M=1 
     and br_dtl.S=1 

)
select * from final

