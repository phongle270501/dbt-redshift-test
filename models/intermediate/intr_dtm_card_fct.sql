{{
    config(
        materialized='table',
        pre_hook = 'truncate table {{this}} ',
        tags = ['intr','dtm', 'fact', 'card_fct']
    )
}}


with

card_smy as (

    select * from {{ ref('card_smy') }} 
    where SNPST_DT = REPLACE('{{ var('etl_date') }}', '-', '')::INT

),
card_dim as (

    select * from {{ ref('card_dim') }}
    where '{{ var("etl_date") }}'::date >= TF_CREATED_AT and '{{ var("etl_date") }}'::date < TF_UPDATED_AT

),
card_ar_dim as (

    select * from {{ ref('card_ar_dim') }}
    where '{{ var("etl_date") }}'::date >= TF_CREATED_AT and '{{ var("etl_date") }}'::date < TF_UPDATED_AT

),
br_dim as (

    select * from {{ ref('br_dim') }}
    where '{{ var("etl_date") }}'::date >= TF_CREATED_AT and '{{ var("etl_date") }}'::date < TF_UPDATED_AT

),
cst_dim as (

    select * from {{ ref('cst_dim') }}
    where '{{ var("etl_date") }}'::date >= TF_CREATED_AT and '{{ var("etl_date") }}'::date < TF_UPDATED_AT

),


final as (
    select
    card.SNPST_DT as CDR_DT,
    card_dim.CARD_DIM_ID as CARD_DIM_ID,
    cst_dim.CST_DIM_ID as CST_DIM_ID,
    card_ar_dim.CARD_AR_DIM_ID as CARD_AR_DIM_ID,
    ou_dim.BR_DIM_ID as OPN_BR_DIM_ID,
    nvl(card.LMT_AMT,0) as LMT_AMT,
    DATEDIFF(DAY, card.EFF_FM_DT,'{{ var("etl_date") }}'::date) as DYS_SINCE_OPN
    from card_smy card 
        inner join card_dim card_dim 
            on card.PYMTC_ID = card_dim.CARD_ID
        inner join card_ar_dim card_ar_dim 
            on card.CARD_AR_ID = card_ar_dim.CARD_AR_ID
        inner join BR_DIM ou_dim 
            on card.OPN_BR_ID = ou_dim.BR_ID
        inner join CST_DIM cst_dim
            on card.CST_ID = cst_dim.CST_ID
)

select * from final

