{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}} ',
        tags=['card_smy']
    )
}}

with card_ar as (
    select * from {{ ref('card_ar') }}
)
, cl_val as (
    select * from {{ ref('cl_val') }}
)
, src_stm as (
    select * from {{ ref('src_stm') }}
)
, ip as (
    select * from {{ ref('ip') }}
)
, card_ar_avy as (
    select * from {{ ref('card_ar_avy') }}
)
, lmt_amt as (
	select *
	from card_ar_avy lmt_amt
	join cl_val lmt_amt_tp on lmt_amt.AVY_TP_ID = lmt_amt_tp.CL_ID
	where lmt_amt_tp.CL_CODE ='W4-CARD-CTR-LMT-AMT'
	and '{{ var("etl_date") }}'::date >= lmt_amt.TF_CREATED_AT AND  '{{ var("etl_date") }}'::date < lmt_amt.TF_UPDATED_AT
)
,unlock_dt as (
	select * 
	from card_ar_avy unlock_dt
	join cl_val unlock_dt_tp on unlock_dt.AVY_TP_ID = unlock_dt_tp.CL_ID
	where unlock_dt_tp.CL_CODE ='W4-CARD-CTR-UNLOCK-DT'
	and '{{ var("etl_date") }}'::date >= unlock_dt.TF_CREATED_AT AND  '{{ var("etl_date") }}'::date < unlock_dt.TF_UPDATED_AT
) 
, ip_x_ip as (
    select * from {{ ref('ip_x_ip') }}
)
,cst_t24 as (
        select *
        from ip_x_ip cst_t24
        join cl_val cst_t24_rltn_tp on cst_t24.IP_X_IP_TP_ID = cst_t24_rltn_tp.CL_ID
        where cst_t24_rltn_tp.CL_CODE ='CST-TO-T24CIF'
        and '{{ var("etl_date") }}'::date >= cst_t24.TF_CREATED_AT AND  '{{ var("etl_date") }}'::date < cst_t24.TF_UPDATED_AT
) 
, final as (
    select ar.CARD_AR_ID as PYMTC_ID
        ,cast(to_char('{{ var("etl_date") }}'::date,'YYYYMMDD') as integer) as SNPST_DT
        ,ar.PRN_AR_ID as CARD_AR_ID
        ,ar.CARD_AR_NM as PYMTC_CODE
        ,prn_ar.CARD_AR_NM as CARD_AR_CODE
        ,lmt_amt.AVY_AMT as LMT_AMT
        ,branch.UNQ_ID_IN_SRC_STM as OPN_BR_CODE
        ,ar.OPN_BR_ID as OPN_BR_ID
        ,ar.UNQ_ID_IN_SRC_STM as UNQ_ID_IN_SRC_STM
        ,unlock_dt.ACT_STRT_DT::date as LAST_ACTVN_DT
        ,cst_t24.OBJ_IP_ID as CST_ID
        ,cst_merge.UNQ_ID_IN_SRC_STM as CST_CODE
        ,ar.EFF_DT::date as EFF_FM_DT
        ,ar.END_DT::date as EFF_TO_DT
        ,source.SRC_STM_CODE as SRC_STM
    from card_ar ar					
    join cl_val cv on ar.AR_TP_ID = cv.CL_ID
    join card_ar prn_ar on ar.PRN_AR_ID = prn_ar.CARD_AR_ID -- prn_ar.AR_ID
    join src_stm source on ar.SRC_STM_ID = source.SRC_STM_ID
    join cl_val status on ar.CARD_LCS_TP_ID = status.CL_ID
    join ip branch on ar.OPN_BR_ID = branch.IP_ID
    join lmt_amt on ar.CARD_AR_ID = lmt_amt.CARD_AR_ID
    join unlock_dt on ar.CARD_AR_ID = unlock_dt.CARD_AR_ID
    join cst_t24 on ar.CST_ID = cst_t24.SBJ_IP_ID
    join ip cst_merge on cst_t24.OBJ_IP_ID = cst_merge.IP_ID
    where cv.CL_CODE = 'W4-CARD-CTR'
)
select * from final

