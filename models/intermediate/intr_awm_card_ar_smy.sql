{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}}',
        tags = ['awm', 'intr_awm_card_ar_smy', 'card_ar_smy']
    )
}}

with card_ar as (
    select * from {{ ref('card_ar') }}
)

, cl_val as(
    select * from {{ ref('cl_val') }}
)

, src_stm as (
    select * from {{ ref('src_stm') }}
)

, ip as (
    select * from {{ ref('ip') }}
)


, odue_dt as (
    select * 
    from {{ ref('card_ar_avy') }} odue_dt
    join {{ ref('cl_val') }} odue_dt_tp 
        on odue_dt.AVY_TP_ID = odue_dt_tp.CL_ID
    where odue_dt_tp.CL_CODE ='W4-ACC-CTR-ODUE-DT' -- loc xong khong con ban ghi
        and '{{ var("etl_date") }}'::date >= odue_dt.TF_CREATED_AT 
        AND '{{ var("etl_date") }}'::date < odue_dt.TF_UPDATED_AT 
)

, card_txn_smy as (
    select txn.TRGT_ANCHOR_ID
          ,sum_txn_amt
    from {{ source('deltalake', 'intr_dbk__card_txn_smy') }} txn
)

, cst_t24 as (
    select * 
    from {{ ref('ip_x_ip')}} cst_t24
        join {{ ref('cl_val')}} cst_t24_rltn_tp
            on cst_t24.IP_X_IP_TP_ID = cst_t24_rltn_tp.CL_ID
    where cst_t24_rltn_tp.CL_CODE ='CST-TO-T24CIF'
        and '{{ var("etl_date") }}'::date >= cst_t24.TF_CREATED_AT
        and  '{{ var("etl_date") }}'::date <  cst_t24.TF_UPDATED_AT
)

, cst_merge as(
    select * from {{ ref('ip') }} 
)       



, final as (
    select 
        ar.CARD_AR_ID CARD_AR_ID
        ,'{{ var("etl_date") }}'::date SNPST_DT
        ,ar.CARD_AR_NM::varchar(64) CARD_AR_CODE
        ,ar.EFF_DT EFF_FM_DT
        ,ar.END_DT EFF_TO_DT
        ,source.SRC_STM_CODE SRC_STM
        ,ar.UNQ_ID_IN_SRC_STM UNQ_ID_IN_SRC_STM
        ,DATEDIFF(DAY, '{{ var("etl_date") }}'::date, odue_dt.ACT_STRT_DT) NBR_ODUE_DAY
        ,status.CL_CODE ::varchar(64) LC_ST
        ,status.CL_NM ::varchar(64) LC_ST_NM
        ,sum_txn_amt TOT_REPYMT_AMT_MTD
        ,ar.OPN_BR_ID OPN_BR_ID
        ,branch.UNQ_ID_IN_SRC_STM::varchar(64) OPN_BR_CODE
        ,cst_t24.OBJ_IP_ID CST_ID
        ,cst_merge.UNQ_ID_IN_SRC_STM CST_CODE
    from card_ar ar
        join cl_val cv 
            on ar.AR_TP_ID = cv.CL_ID 
            and cv.CL_CODE = 'W4-ACC-CTR'
        join src_stm source
            on ar.SRC_STM_ID = source.SRC_STM_ID
        join cl_val as "status"
            on ar.CARD_LCS_TP_ID = status.CL_ID
        join ip branch
            on ar.OPN_BR_ID = branch.IP_ID
        left join odue_dt
            on ar.CARD_AR_ID = odue_dt.CARD_AR_ID
        left join card_txn_smy txn
            on ar.CARD_AR_ID = txn.TRGT_ANCHOR_ID
        join cst_t24
            on ar.CST_ID = cst_t24.SBJ_IP_ID
        join cst_merge
            on cst_t24.OBJ_IP_ID = cst_merge.IP_ID
)
select * from final
