{% set path = model.path %}
{% set column_names = mcr_get_bussiness_columns(this, 'card_ar_dim_id') %}

{{
    config(
        materialized = 'scd',
        unique_key= ['card_ar_id', 'tf_created_at'],
        identity_col = 'card_ar_dim_id',
        strategy = 'type2',
        post_hook="{{ create_data_masked_view(
                            schema='public_analytics'
                      ) }}",
        tags=['card']
    )
}}

with source as (

    select ar.card_ar_code card_ar_nbr
        ,ar.card_ar_id
        ,ar.src_stm
        ,ar.unq_id_in_src_stm::integer
        ,ar.eff_to_dt eff_mat_dt
        ,ar.lc_st
    from {{ ref('card_ar_smy')}}  ar
    where  snpst_dt = REPLACE('{{ var('etl_date') }}', '-', '')::INT

)
, source_rows as (
  
    select  ar.card_ar_nbr
            ,ar.card_ar_id
            ,ar.src_stm
            ,ar.unq_id_in_src_stm
            ,ar.eff_mat_dt
            ,ar.lc_st
           -------------technical columns-----------      
           , '{{ var("etl_date") }}'::date as tf_created_at
           , '{{ var("end_date") }}'::timestamp as tf_updated_at
           , {{dbt_date.today()}} ppn_dt
           , {{dbt_date.now()}}::time ppn_tm
           , {{ dbt_utils.generate_surrogate_key(  column_names )}} compare_key
     from source ar
  
)


{% if is_incremental() %}
    , destination_rows as (---lay cac ban ghi co hieu luc o thoi diem hien tai    
        select *
               , {{ dbt_utils.generate_surrogate_key(  column_names)}} compare_key
         from {{ this }} 
        where tf_updated_at = cast('{{ var("end_date") }}' as timestamp)
    
    )

    , new_valid_to as (---xac dinh thong tin ban ghi moi duoc insert vao    
        select   s.card_ar_nbr
                ,s.card_ar_id
                ,s.src_stm
                ,s.unq_id_in_src_stm
                ,s.eff_mat_dt
                ,s.lc_st
                , s.ppn_dt, s.ppn_tm
                , s.tf_created_at
                , s.tf_updated_at  -- thong tin la thong tin moi nhat tu Source, hieu luc tu ngay Etl -> oo                         
          from source_rows s
          left join destination_rows d
            on s.card_ar_id = d.card_ar_id
         where nvl(s.compare_key, '$')  != nvl(d.compare_key, '$')      
    )

    , add_new_valid_to as (---End date ban ghi cu
        select   d.card_ar_nbr
                ,d.card_ar_id
                ,d.src_stm
                ,d.unq_id_in_src_stm
                ,d.eff_mat_dt
                ,d.lc_st               
                , d.ppn_dt, d.ppn_tm
                , d.tf_created_at
                , '{{ var("etl_date") }}'::timestamp as tf_updated_at  ---end date
          from destination_rows d
          join new_valid_to n
            on d.card_ar_id = n.card_ar_id

    )
    , final as (
        select n.*
        from add_new_valid_to n
        union 
        select o.* from new_valid_to o
    )
{% endif %}

select   s.card_ar_nbr
        ,s.card_ar_id
        ,s.src_stm
        ,s.unq_id_in_src_stm
        ,s.eff_mat_dt
        ,s.lc_st
        ,s.ppn_dt
        ,s.ppn_tm
        ,s.tf_created_at
        ,s.tf_updated_at
        ,'{{ model.path }}' job_nm
        ,{{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
  from source_rows s
{% if is_incremental() %}
    Where 1=2 
    union all 
    Select f.*,'{{ model.path }}' job_nm,{{ dbt_utils.generate_surrogate_key( column_names )}} compare_key from final f
    
{% endif %}