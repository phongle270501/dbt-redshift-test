{#
    job name : cst_dim
    mapping ID : DM CST_DIM
    created by : tien.cm  
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update cdode
#}

{% set column_names = mcr_get_bussiness_columns(this, 'cst_dim_id') %}
--- config job
{{
    config(
        materialized = 'scd',
        full_refresh = false,
        unique_key=['cst_id'],
        identity_col = 'cst_dim_id',
        strategy = 'type2',
        pre_hook = scd2_revert('datamart', this),     
        tags=['dtm', 'dim', 'cst_dim']
    )
}}

--- declare all model in use
with source as (

    select source.src_stm_code, source.src_stm_id
    from {{ ref('src_stm')}}  source
    where source.src_stm_code in ( 'T24_CUSTOMER','WAY4_CLIENT')

)

, ip as (
    select 
        ip.ip_id
        ,ip.src_stm_id
        ,ip.unq_id_in_src_stm
    from {{ ref('ip')}} ip
)

, segment as (
    select a.cst_id,a.cl_id
    from {{ ref('cl_x_cst') }} a
    where '{{ var("etl_date") }}'::timestamp between a.tf_created_at and a.tf_updated_at
    
)

, cl_val as (
    select * from {{ref('cl_val')}}
)

, temp as (
    select 
        ip.ip_id as cst_id
        ,source.src_stm_code as src_stm
        ,ip.unq_id_in_src_stm
        ,segment_value.cl_code seg_code
        , '{{ var("etl_date") }}'::timestamp as tf_created_at
    from ip
    join source on ip.src_stm_id  = source.src_stm_id 
    left join segment on ip.ip_id = segment.cst_id
    left join cl_val segment_value on segment.cl_id = segment_value.cl_id
)
, source_rows as (
  
    select   {{'a.'~column_names| join(',a.')}}
           -------------technical columns----------- 
           , a.tf_created_at                
           , {{dbt_date.today()}} ppn_dt
           , {{dbt_date.now()}}::time ppn_tm
           , '{{ var("end_date") }}'::timestamp as tf_updated_at
           , '{{model.path}}' job_nm
           , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
     from temp a
  
)

--- final data
select s.*
  from source_rows s

