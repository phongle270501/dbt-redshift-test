{{ config(materialized='view') }}

with source as 
(
    select * from {{ source('t24', 't24_customer_details') }}
	where t24_load_date = to_char('{{ var("etl_date") }}'::date, 'dd-Mon-YYYY')	
),


renamed as
(
    select 
		customer_code	, --::	decimal(32,5)	,
		m	::	decimal(32,5)	,
		s	::	decimal(32,5)	,
		efz_co_code	::	varchar(256)	,
		t24_load_date	::	date	,
		efz_load_date	::	date	,
		short_name	::	varchar(256)	,
		name_1	::	varchar(256)	,
		name_2	::	varchar(256)	,
		street	::	varchar(256)	,
		address	::	varchar(256)	,
		town_country	::	varchar(256)	,
		post_code	::	varchar(256)	,
		country	::	varchar(256)	,
		relation_code	::	decimal(32,5)	,
		rel_customer	::	decimal(32,5)	,
		revers_rel_code	::	decimal(32,5)	,
		rel_deliv_opt	::	varchar(256)	,
		other_officer	::	varchar(256)	,
		text	::	varchar(256)	,
		legal_id	::	varchar(256)	,
		legal_doc_name	::	varchar(256)	,
		legal_holder_name	::	varchar(256)	,
		legal_iss_auth	::	varchar(256)	,
		legal_iss_date	::	varchar(256)	,
		legal_exp_date	::	varchar(256)	,
		off_phone	::	varchar(256)	,
		posting_restrict	::	varchar(256)	,
		customer_rating	::	varchar(256)	,
		cr_profile_type	::	varchar(256)	,
		cr_profile	::	varchar(256)	,
		phone_1	::	varchar(256)	,
		sms_1	::	varchar(256)	,
		email_1	::	varchar(256)	,
		addr_location	::	varchar(256)	,
		employment_status	::	varchar(256)	,
		occupation	::	varchar(256)	,
		job_title	::	varchar(256)	,
		employers_name	::	varchar(256)	,
		employers_add	::	varchar(256)	,
		employers_buss	::	varchar(256)	,
		employment_start	::	varchar(256)	,
		customer_currency	::	varchar(256)	,
		salary	::	varchar(256)	,
		annual_bonus	::	varchar(256)	,
		salary_date_freq	::	varchar(256)	,
		residence_status	::	varchar(256)	,
		residence_type	::	varchar(256)	,
		residence_since	::	varchar(256)	,
		residence_value	::	varchar(256)	,
		mortgage_amt	::	varchar(256)	,
		other_fin_rel	::	varchar(256)	,
		other_fin_inst	::	varchar(256)	,
		comm_type	::	varchar(256)	,
		pref_channel	::	varchar(256)	,
		legal_id_doc_name	::	varchar(256)	,
		interests	::	varchar(256)	,
		fax_1	::	varchar(256)	,
		previous_name	::	varchar(256)	,
		change_date	::	varchar(256)	,
		change_reason	::	varchar(256)	,
		further_details	::	varchar(256)	,
		other_nationality	::	varchar(256)	,
		tax_id	::	varchar(256)	,
		vis_type	::	varchar(256)	,
		vis_comment	::	varchar(256)	,
		vis_internal_review	::	varchar(256)	,
		former_vis_type	::	varchar(256)	,
		former_vis_comment	::	varchar(256)	,
		risk_asset_type	::	varchar(256)	,
		risk_level	::	varchar(256)	,
		risk_tolerance	::	varchar(256)	,
		risk_from_date	::	varchar(256)	,
		mandate_appl	::	varchar(256)	,
		mandate_reg	::	varchar(256)	,
		mandate_record	::	varchar(256)	,
		holdings_pivot	::	varchar(256)	,
		alt_cus_id	::	varchar(256)	,
		extern_sys_id	::	varchar(256)	,
		social_ntw_ids	::	varchar(256)	,
		cr_user_profile_type	::	varchar(256)	,
		cr_calc_profile	::	varchar(256)	,
		cr_user_profile	::	varchar(256)	,
		cr_calc_reset_date	::	varchar(256)	,
		contact_type	::	varchar(256)	,
		contact_num	::	varchar(256)	,
		email_addr	::	varchar(256)	,
		doc_type	::	varchar(256)	,
		doc_num	::	varchar(256)	,
		doc_issue_place	::	varchar(256)	,
		doc_issue_date	::	decimal(32,5)	,
		doc_expiry_date	::	decimal(32,5)	,
		office_address	::	varchar(256)	,
		per_addr_street	::	varchar(256)	,
		cu_rel_name	::	varchar(256)	,
		cu_rel_legalid	::	varchar(256)	,
		cu_rel_pos	::	varchar(256)	,
		date_card_type	::	varchar(256)	,
		"partition"	::	varchar(256)	,
		supplier_id	::	varchar(256)	,
		bom_mem_name	::	varchar(256)	,
		bom_mem_address	::	varchar(256)	,
		bom_mem_id	::	varchar(256)	,
		promotion_prg	::	varchar(256)	,
		cu_rel_mobile	::	varchar(256)	,
		other_rel_type	::	varchar(256)	,
		other_rel_name	::	varchar(256)	,
		other_legal_id	::	varchar(256)	,
		other_doc_id	::	varchar(256)	,
		from_date	::	varchar(256)	,
		to_date	::	varchar(256)	,
		vpb_nguyenquan	::	varchar(256)	,
		vp_mobile	::	varchar(256)	,
		vp_leg_date	::	varchar(256)	,
		vp_iss_date	::	decimal(32,5)	,
		vp_issue_date	::	decimal(32,5)	,
		vpb_field_key	::	varchar(256)	,
		vpb_field_value	::	varchar(256)	,
		emailid	::	varchar(256)	,
		vpb_add_code	::	varchar(256)	,
		vpb_add_value	::	varchar(256)	,
		inputter	::	varchar(256)	,
		date_time	::	bigint	,
		vp_email1	::	varchar(256)	,
		vp_email2	::	varchar(256)	,
		co_code	::	varchar(256)	,
		act_dt	::	date	,
		act_key	::	varchar(256)	,
		ppn_dt	::	date	,
		ppn_time	::	time	
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final


