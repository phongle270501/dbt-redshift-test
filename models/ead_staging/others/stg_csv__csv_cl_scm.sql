{{ config(materialized='view') }}


with source as 
(
    select * from {{ source('csv', 'csv_cl_scm') }}
),

renamed as
(
    select 
        SCM_CODE :: VARCHAR(64)
        ,shrt_nm :: VARCHAR(50)
        ,cl_scm_nm :: VARCHAR(100)
        ,dsc :: VARCHAR(200)
        ,eff_dt :: date
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final

