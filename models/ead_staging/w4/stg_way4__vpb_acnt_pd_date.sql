{{ config(materialized='view') }}

with source as 
(
    select * from {{ source('w4', 'way4_vpb_acnt_pd_date') }}
    Where business_date =  to_char('{{ var("etl_date") }}'::date, 'yyyyMMdd')
),

renamed as
(
    select 
        business_date :: INTEGER   
        ,trim(substring(id, 0, POSITION('.' IN id )))::INTEGER  as id
        ,local_date :: VARCHAR(256)   
        ,trim(substring(acnt_contract__oid, 0, POSITION('.' IN acnt_contract__oid )))::INTEGER  as acnt_contract__oid 
        ,contract_number :: VARCHAR(256)   
        ,trim(SUBSTRING(pd_date FROM 1 FOR 10))::date pd_date
        ,pd_date_1 :: VARCHAR(256)   
        --,dpd :: INTEGER   
        ,trim(substring(dpd, 0, POSITION('.' IN dpd )))::INTEGER  as dpd 
        ,dpd_1 :: VARCHAR(256)   
        ,pd_level :: VARCHAR(256)   
        ,baddebt_date :: VARCHAR(256)   
        ,pd_status :: VARCHAR(256)   
        ,amnd_state :: VARCHAR(256)   
        ,amnd_date :: VARCHAR(256)   
        ,act_dt :: DATE   
        ,act_key :: VARCHAR(256)   
        ,ppn_dt :: DATE   
        ,ppn_time :: TIME
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final