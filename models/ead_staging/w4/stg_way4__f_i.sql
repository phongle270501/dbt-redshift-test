{{ config(materialized='view') }}

with source as 
(
    select * from {{ source('w4', 'way4_f_i') }}
	WHERE business_date =  to_char('{{ var("etl_date") }}'::date, 'yyyyMMdd')
),

renamed as
(
    select 
		business_date::DECIMAL(32,5)	,
		amnd_state::VARCHAR(256)	,
		amnd_date::DATE	,
		amnd_officer::DECIMAL(32,5)	,
		amnd_prev::DECIMAL(32,5)	,
		id::DECIMAL(32,5)	,
		name::VARCHAR(256)	,
		bank_code::VARCHAR(256)	,
		branch_code::VARCHAR(256)	,
		cb_code::VARCHAR(256)	,
		sundry_contract::DECIMAL(32,5)	,
		posting_in::DECIMAL(32,5)	,
		min_deposit::DOUBLE PRECISION	,
		expence_percent::DOUBLE PRECISION	,
		deposit_contract::DECIMAL(32,5)	,
		liab_contract::VARCHAR(256)	,
		last_scan::VARCHAR(256)	,
		local_currency::DECIMAL(32,5)	,
		days_in_year::DECIMAL(32,5)	,
		country::VARCHAR(256)	,
		mirror_scheme::VARCHAR(256)	,
		time_zone::DECIMAL(32,5)	,
		local_date::DATE	,
		ext_local_date::VARCHAR(256)	,
		interest_in_cycle::VARCHAR(256)	,
		parent_fi::DECIMAL(32,5)	,
		post_due::VARCHAR(256)	,
		numeration_scheme::VARCHAR(256)	,
		calendar_type::VARCHAR(256)	,
		prnt_routing::VARCHAR(256)	,
		cr_lim_posting::VARCHAR(256)	,
		fx_in_ho::VARCHAR(256)	,
		special_parms::VARCHAR(256)	,
		tariff_domain::VARCHAR(256)	,
		bank_client::VARCHAR(256)	,
		unit::VARCHAR(256)	,
		act_dt::DATE	,
		act_key::VARCHAR(256)	,
		ppn_dt::DATE	,
		ppn_time::TIME	
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final


