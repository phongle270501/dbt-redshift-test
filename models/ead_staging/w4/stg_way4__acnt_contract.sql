{{ config(materialized='table') }}

with source as 
(
    select * from {{ source('w4', 'way4_acnt_contract') }}
	where act_dt =  to_char('{{ var("etl_date") }}'::date, 'yyyy-MM-dd')	
),


renamed as
(
    select 
		is_ready::VARCHAR(256),
		trim(substring(acc_scheme__id, 0, POSITION('.' IN acc_scheme__id )))::INTEGER as acc_scheme__id,
		trim(substring(acnt_contract__oid, 0, POSITION('.' IN acnt_contract__oid )))::INTEGER as acnt_contract__oid,
		add_info_04::VARCHAR(256),
		amnd_state::VARCHAR(256),
		base_relation::VARCHAR(256),
		card_expire::INTEGER,
		trim(substring(client__id, 0, POSITION('.' IN client__id )))::INTEGER as client__id,
		contract_number::VARCHAR(256),
		trim(substring(contr_status, 0, POSITION('.' IN contr_status )))::INTEGER as contr_status,
		trim(substring(contr_subtype__id, 0, POSITION('.' IN contr_subtype__id )))::INTEGER as contr_subtype__id,
		con_cat::VARCHAR(256),
		curr::INTEGER,
		date_expire::VARCHAR(256),
		date_open::VARCHAR(256),
		trim(substring(f_i, 0, POSITION('.' IN f_i )))::INTEGER as f_i,
		trim(substring(id, 0, POSITION('.' IN id )))::INTEGER as id,
		product::DOUBLE PRECISION,
		rbs_number::VARCHAR(256),
		t24_al_id::VARCHAR(256),
		t24_dao::VARCHAR(256),
		trim(substring(amnd_officer, 0, POSITION('.' IN amnd_officer )))::INTEGER as amnd_officer,
		trim(substring(acnt_contract__id, 0, POSITION('.' IN acnt_contract__id )))::INTEGER as acnt_contract__id,
		amnd_date::DATE,
		amnd_date_time::VARCHAR(256),
		amount_available::DOUBLE PRECISION,
		auth_limit_amount::DOUBLE PRECISION,
		base_auth_limit::DOUBLE PRECISION,
		ccat::VARCHAR(256),
		channel::VARCHAR(256),
		contract_name::VARCHAR(256),
		trim(substring(contr_type, 0, POSITION('.' IN contr_type )))::INTEGER as contr_type,
		liab_balance::DOUBLE PRECISION,
		liab_blocked::DOUBLE PRECISION,
		trim(substring(main_product, 0, POSITION('.' IN main_product )))::INTEGER as liab_contract,
		limit_is_active::VARCHAR(256),
		trim(substring(main_product, 0, POSITION('.' IN main_product )))::INTEGER as main_product,
		merchant_id::VARCHAR(256),
		next_billing_date::VARCHAR(256),
		risk_scheme::VARCHAR(256),
		service_group::VARCHAR(256),
		sub_balance::DOUBLE PRECISION,
		sub_blocked::DOUBLE PRECISION,
		total_balance::DOUBLE PRECISION,
		total_blocked::DOUBLE PRECISION,
		pcat::VARCHAR(256),
		act_dt::DATE,
		act_key::VARCHAR(256),
		ppn_dt::DATE,
		ppn_time::TIME,
		add_info_02::VARCHAR(256),
		last_billing_date::VARCHAR(256),
		contract_number_hash::VARCHAR(256),
		comment_text::VARCHAR(256),
		tr_sic::VARCHAR(256)
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final


