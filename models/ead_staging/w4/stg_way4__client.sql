{{ config(materialized='view') }}

with source as 
(
    select * from {{ source('w4', 'way4_client') }}
	Where business_date =  to_char('{{ var("etl_date") }}'::date, 'yyyyMMdd')	
),


renamed as
(
    select 
		amnd_state	::	varchar(256)	,
		trim(substring(id, 0, POSITION('.' IN id )))::INTEGER  as id,
		--id	::	varchar(256)	,
		short_name	::	varchar(256)	,
		reg_number_type	::	varchar(256)	,
		reg_number	::	varchar(256)	,
		client_number	:: varchar(256), --::	double precision	,
		last_nam	::	varchar(256)	,
		first_nam	::	varchar(256)	,
		gender	::	varchar(256)	,
		birth_date	::	varchar(256)	,
		date_open	::	date	,
		act_dt	::	date	,
		act_key	::	varchar(256)	,
		ppn_dt	::	date	,
		ppn_time	::	time	,
		country	::	varchar(256)	,
		business_date	::	decimal(32,5)	,
		phone_h	::	varchar(256)	,
		father_s_nam	::	varchar(256)	,
		address_zip	::	varchar(256)	,
		birth_nam	::	varchar(256)	,
		company_nam	::	varchar(256)	,
		url	::	varchar(256)	,
		tr_title	::	varchar(256)	,
		service_group	::	varchar(256)	,
		is_ready	::	varchar(256)	,
		address_line_3	::	varchar(256)	,
		trim(substring(clt, 0, POSITION('.' IN clt )))::INTEGER  as clt,
		--clt	::	decimal(32,5)	,
		amnd_date	::	date	,
		city	::	varchar(256)	,
		language	::	varchar(256)	,
		birth_place	::	varchar(256)	,
		trim(substring(amnd_prev, 0, POSITION('.' IN amnd_prev )))::INTEGER  as amnd_prev,
		--amnd_prev	::	decimal(32,5)	,
		address_line_4	::	varchar(256)	,
		phone	::	varchar(256)	,
		tr_company_nam	::	varchar(256)	,
		company_department	::	varchar(256)	,
		date_expire	::	varchar(256)	,
		pcat	::	varchar(256)	,
		trade_nam	::	varchar(256)	,
		enable_affiliation	::	varchar(256)	,
		e_mail	::	varchar(256)	,
		trim(substring(f_i, 0, POSITION('.' IN f_i )))::INTEGER  as f_i,
		--f_i	::	decimal(32,5)	,
		address_line_1	::	varchar(256)	,
		tr_first_nam	::	varchar(256)	,
		delivery_type	::	varchar(256)	,
		profession	::	varchar(256)	,
		reg_details	::	varchar(256)	,
		ccat	::	varchar(256)	,
		state	::	varchar(256)	,
		phone_m	::	varchar(256)	,
		branch	::	decimal(32,5)	,
		fax	::	varchar(256)	,
		fax_h	::	varchar(256)	,
		tax_position	::	varchar(256)	,
		tr_last_nam	::	varchar(256)	,
		trim(substring(amnd_officer, 0, POSITION('.' IN amnd_officer )))::INTEGER  as amnd_officer,
		--amnd_officer	::	decimal(32,5)	,
		salutation_suffix	::	varchar(256)	,
		citizenship	::	varchar(256)	,
		trim(substring(title, 0, POSITION('.' IN title )))::INTEGER  as title,
		--title	::	decimal(32,5)	,
		mother_s_nam	::	varchar(256)	,
		trim(substring(marital_status, 0, POSITION('.' IN marital_status )))::INTEGER  as marital_status
		--marital_status	::	decimal(32,5)	
    from source
),

final as
(
    select
     	*
    from renamed
)

select * from final


