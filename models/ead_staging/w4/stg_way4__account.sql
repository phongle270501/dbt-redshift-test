{#
    job name : stg_way4__account
    created by : phong.le
    modified by | date modify | description
    phong.le    | 2023-01-16  | create job
#}

with source as (
    select * from {{source('w4', 'way4_account')}}
    Where business_date =  to_char('{{ var("etl_date") }}'::date, 'yyyyMMdd')	
),

rename as (
    select 
        acnt_contract__oid :: varchar(256)
        ,code :: varchar(256)
        ,interest_rate :: decimal(5,2)
        ,id :: varchar(256)
        ,curr :: varchar(256)
        ,acat :: varchar(256)
        ,account_type :: varchar(256)
        ,is_am_available :: varchar(256)
        ,account_number :: varchar(256)
        ,acc_templ__id :: varchar(256)
        ,account_name :: varchar(256)
        ,cycle_date_to :: date
        ,current_balance :: decimal(32,5)
        ,alter_account :: varchar(256)
        ,n_of_cycle :: varchar(256)
        ,gl_number :: varchar(256)
        ,top_account :: varchar(256)
        ,ageing_priority :: varchar(256)
        ,on_date :: date
        ,on_date_balance :: decimal(32,5)
        ,business_date :: integer
        ,etl_dt :: timestamp 
        ,act_dt :: date
        ,act_key :: varchar(256)
        ,ppn_dt :: date
        ,ppn_time :: time
    from source
)

select * from rename