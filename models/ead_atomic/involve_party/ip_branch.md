{#
    job name : ip
    mapping ID : EAD_RA IP
    created by : LY.TRINH
    modified by | date modify | description
    LY.TRINH   | 2023-09-04  | update code
#}

{% set column_names = mcr_get_bussiness_columns(this) %}
{% set column_names_v2 = mcr_get_bussiness_columns_v2(this, 't') %}
{% set hook_tbl = 'atomic.ip_async' %}
{% set hook_src_stm_nm = 'T24_VPB_COMPANY' %}

{%- set actions = process_in_target_table( this, hook_tbl,  'scd1', ['ip_id']) -%}

--- config job
{{
    config(
        materialized='incremental',
        pre_hook = 'truncate table {{this}} ',
        post_hook =  actions ,
        tags = ['awm', 'ip_async']
    )
}}
--- declare all model in use
with t24_branch as (
    select * 
    from {{ ref('intr_awm_ip__t24_branch') }} where TF_CREATED_AT = '{{ var("etl_date") }}'::date
)
,src_stm as(
    select * from {{ ref('src_stm') }}  
)
,final as (

    select 
        a.*
        , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
        , '{{ var("end_date") }}'::date as tf_updated_at
    from t24_branch a
)
--- process if is daily run
{% if is_incremental() %}
 ,trgt as(
    select t.*, {{ dbt_utils.generate_surrogate_key( column_names_v2 )}} compare_key 
    from {{hook_tbl}} t
    join src_stm  src
     ON t.src_stm_id = src.src_stm_id and src.src_stm_code = '{{hook_src_stm_nm}}'
)
{% endif %}

select 
     {{'s.'~column_names| join(',s.')}}
     ------------------------------
    ,'{{ model.path }}' job_nm
    , s.tf_created_at 
    , s.tf_updated_at    
    ,{{dbt_date.today()}} ppn_dt
    ,{{dbt_date.now()}}::time ppn_tm 
    , null::date tf_deleted_at
from final s
--- process if is daily run
{% if is_incremental() %}
   left join trgt t ON s.ip_id = t.ip_id
   where nvl(t.compare_key,'$') <> nvl(s.compare_key,'$')
   {# --process 'D' flag  -#}
   union all 
   select {{'t.'~column_names| join(',t.')}}
        --------------------------------------
        , t.job_nm
        , t.tf_created_at 
        , t.tf_updated_at    
        , {{dbt_date.today()}} ppn_dt
        , {{dbt_date.now()}}::time ppn_tm 
        , '{{ var("etl_date") }}'::date as tf_deleted_at
   from trgt t 
   left join final s ON s.ip_id = t.ip_id
   where s.ip_id is null
{% endif %}
