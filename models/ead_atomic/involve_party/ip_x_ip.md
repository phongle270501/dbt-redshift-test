{#
    job name : ip_x_ip
    mapping ID : EAD_RA IP_X_IP
    created by : tien.cu
    modified by | date modify | description
    tien.cu     | 2023-09-04  | update code
#}
{% set column_names = mcr_get_bussiness_columns(this) %}

--- config job
{{ 
    config(
        materialized='incremental',
        unique_key=['SBJ_IP_ID', 'IP_X_IP_TP_ID' , 'tf_created_at'],
        incremental_strategy='delete+insert',
        pre_hook = scd2_revert('ead_awm', this),        
        tags = ['awm', 'ip_x_ip']
    ) 
}}
--- declare all model in use
with intr as (

    select s.*
    from {{ ref('intr_awm_ip_x_ip__customer_merge') }}  s where TF_CREATED_AT = '{{ var("etl_date") }}'

)
, intr_row as (
  
    select  a.*
           ,{{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
           ,'{{ model.path }}' job_nm
           ,'{{ var("end_date") }}'::timestamp as tf_updated_at
           ,{{dbt_date.today()}} ppn_dt
           ,{{dbt_date.now()}}::time ppn_tm
     from intr a
)

--- process if is daily run
{% if is_incremental() %}
    , destination_rows as (---lay cac ban ghi co hieu luc o thoi diem hien tai    
        select *
               --, md5(concat(user_id, cast('{{ var("etl_date") }}' as timestamp))) as cust_dim_code 
               , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
         from {{ this }} 
        where tf_updated_at = cast('{{ var("end_date") }}' as timestamp)
    
    )
    , new_valid_to as (---xac dinh thong tin ban ghi moi duoc insert vao    
        select {{'s.'~column_names| join(',s.')}}
            , s.job_nm
            , s.tf_created_at 
            , s.tf_updated_at  ---end date
            , s.ppn_dt
            , s.ppn_tm  -- thong tin la thong tin moi nhat tu Source, hieu luc tu ngay Etl -> oo                         
          from intr_row s
          left join destination_rows d
            on s.SBJ_IP_ID = d.SBJ_IP_ID and  s.IP_X_IP_TP_ID = d.IP_X_IP_TP_ID 
         where nvl(s.compare_key, '$')  != nvl(d.compare_key, '$')      
    )

    , add_new_valid_to as (---End date ban ghi cu
        select  {{'d.'~column_names| join(',d.')}}
                , d.job_nm
                , d.tf_created_at                 
                , '{{ var("etl_date") }}'::timestamp as tf_updated_at  ---end date
                , n.ppn_dt
                , n.ppn_tm
          from destination_rows d
          join new_valid_to n
            on d.SBJ_IP_ID = n.SBJ_IP_ID and  d.IP_X_IP_TP_ID = n.IP_X_IP_TP_ID 
    )
    , not_exists_in_src as (  
        select  {{'d.'~column_names| join(',d.')}}
                , d.job_nm
                , d.tf_created_at                 
                , '{{ var("etl_date") }}'::timestamp as tf_updated_at  ---end date
                ,{{dbt_date.today()}} ppn_dt
                ,{{dbt_date.now()}}::time ppn_tm
          from destination_rows d
          left join intr_row n
            on d.SBJ_IP_ID = n.SBJ_IP_ID and  d.IP_X_IP_TP_ID = n.IP_X_IP_TP_ID 
          where n.SBJ_IP_ID is null and n.IP_X_IP_TP_ID is null
    )
    
    , final as (
        select n.*
        from add_new_valid_to n
        union all
        select o.* from new_valid_to o
        union all
        select d.* from not_exists_in_src d
    )
{% endif %}

--- final data
select   
    {{'d.'~column_names| join(',d.')}}
        -----------------
    , d.tf_created_at 
    , d.tf_updated_at  ---end date
    , d.ppn_dt
    , d.ppn_tm
    , d.job_nm
    from intr_row d
  --- process if is daily run
{% if is_incremental() %}
    Where 1=2 
    union all 
    Select 
    {{'s.'~column_names| join(',s.')}}
    ----------------------
    , s.tf_created_at 
    , s.tf_updated_at  ---end date
    , s.ppn_dt
    , s.ppn_tm
    , s.job_nm 
    from final s
    
{% endif %}
