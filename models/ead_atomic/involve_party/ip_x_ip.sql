{#
    job name : ip_x_ip
    mapping ID : EAD_RA IP_X_IP
    created by : tien.cu
    modified by | date modify | description
    tien.cu     | 2023-09-04  | update code
#}
{% set column_names = mcr_get_bussiness_columns(this) %}

--- config job
{{ 
    config(
        materialized='scd',
        unique_key=['sbj_ip_id', 'ip_x_ip_tp_id'],
        strategy = 'type2',
        pre_hook = scd2_revert('atomic', this),  
        full_refresh = false,      
        tags = ['awm', 'ip_x_ip']
    ) 
}}

--- declare all model in use
with intr as (

    select s.*
    from {{ ref('intr_awm_ip_x_ip__customer_merge') }}  s where TF_CREATED_AT = '{{ var("etl_date") }}'

)
, intr_row as (
  
    select  {{'a.'~ column_names| join(',a.')}}          
           ,'{{ model.path }}' job_nm
           ,'{{ var("etl_date") }}'::timestamp as tf_created_at
           ,'{{ var("end_date") }}'::timestamp as tf_updated_at
           ,{{ dbt_date.today() }} ppn_dt
           ,{{ dbt_date.now() }}::time ppn_tm
           ,{{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
     from intr a
)
--- final data
select   
    s.*
from intr_row s

