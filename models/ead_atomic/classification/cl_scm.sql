{#
    job name : cl_scm
    mapping ID : EAD_RA CL_SCM
    created by : tai
    modified by | date modify | description
    tien.cu    | 2023-09-04  | update
#}

{% set column_names = mcr_get_bussiness_columns(this) %}
--- config job
{{
    config(
        materialized = 'incremental',
        unique_key = 'CL_SCM_ID',
        tags = ["awm_cl_scm", "cl", "cl_scm"]
    )
}}

--- declare all model in use
with csv_cl_scm as (
    select * from {{ ref('stg_csv__csv_cl_scm') }}
),

final as (
    select 
            {{ mcr_generate_surrogate_key([{'column': 'cl.scm_code'} , {'text': 'CSV_CL_SCM'}]) }} as cl_scm_id,
            {{ mcr_generate_surrogate_key([{'text': 'CSV_CL_SCM'} , {'text': 'CSV_SRC_STM'}]) }} as src_stm_id,
            cl.scm_code::varchar as scm_code,
            cl.shrt_nm::varchar as shrt_nm,            
            ---technical col
            '{{ var("etl_date") }}'::date as tf_created_at,
            '{{ var("end_date") }}'::date as tf_updated_at,
            ---compare column
            {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
     from csv_cl_scm cl
)
--- process if is daily run
{% if is_incremental() %}
    ,trgt as(
        select  t.*, {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key 
          from  {{this}} t
    )
{% endif %}

--- final data
select 
    {{'s.'~column_names| join(',s.')}}
    ------------------------------------
    , '{{ model.path }}' job_nm
    --- process if is daily run
    {% if is_incremental() %}
        , COALESCE(t.tf_created_at, s.tf_created_at) tf_created_at
        , case when t.CL_SCM_ID is null then s.tf_updated_at else '{{ var("etl_date") }}'::date end  tf_updated_at   
    {% else %}
        , s.tf_created_at
        , s.tf_updated_at
    {% endif %}    
    , {{dbt_date.today()}} ppn_dt
    , {{dbt_date.now()}}::time ppn_tm    
from final s
--- process if is daily run
{% if is_incremental() %}
   left join trgt t ON s.cl_scm_id = t.cl_scm_id
   where nvl(t.compare_key,'$') <> nvl(s.compare_key,'$')
{% endif %}
