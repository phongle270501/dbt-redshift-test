{#
    job name : src_stm
    mapping ID : EAD_RA SRC_STM
    created by : nhung
    modified by | date modify | description
    tien.cu    | 2023-09-04  | update code
#}

{% set column_names = mcr_get_bussiness_columns(this) %}
--- config job
{{
    config(
        materialized = 'incremental',
        unique_key = 'SRC_STM_ID',
        tags = ["awm_src_stm", "cl", "src_stm"]
    )
}}

--- declare all model in use
with source as (    
    select * from {{ ref('stg_csv__csv_src_stm') }}
)

, transform as (
    select    {{ mcr_generate_surrogate_key([{'column': 'a.SRC_STM_CODE'}, {'text': 'CSV_SRC_STM'}]) }} src_stm_id
            , src_stm_nm ::varchar(64)      src_stm_nm 
            , src_stm_code::varchar(64)     src_stm_code      
            , case when a.prn_src_stm_id is not null then {{ mcr_generate_surrogate_key([{'column': 'a.prn_src_stm_id'}, {'text': 'CSV_SRC_STM'}]) }} 
                   else null 
               end prn_src_stm_id    
            , eff_dt ::date     eff_dt
            , end_dt ::date     end_dt
            , '{{ var("etl_date") }}'::date as tf_created_at
            , '{{ var("end_date") }}'::date as tf_updated_at         
            , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
    from source a 
)
--- process if is daily run
{% if is_incremental() %}
 ,trgt as(
    select  t.*
        ,{{ dbt_utils.generate_surrogate_key( column_names )}} compare_key 
    from {{this}} t
)
{% endif %}
--- final data
select 
    {{'s.'~column_names| join(',s.')}}
    ---------------------------
     , '{{ model.path }}' job_nm
     --- process if is daily run
     {% if is_incremental() %}
        , COALESCE(t.tf_created_at, s.tf_created_at) tf_created_at
        , case when t.src_stm_id is null then s.tf_updated_at else '{{ var("etl_date") }}'::date end  tf_updated_at   
     {% else %}
        , s.tf_created_at
        , s.tf_updated_at
     {% endif %}    
     ,{{dbt_date.today()}} ppn_dt
     ,{{dbt_date.now()}}::time ppn_tm     
from transform s
--- process if is daily run
{% if is_incremental() %}
   left join trgt t ON s.src_stm_id = t.src_stm_id
   where nvl(t.compare_key,'$') <> nvl(s.compare_key,'$')
{% endif %}