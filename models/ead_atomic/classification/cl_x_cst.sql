{#
    job name : cl_x_cst
    mapping ID : EAD_RA CL_X_CST CustomerSegment
    created by : duc
    modified by | date modify | description
    tien.cu    | 2023-09-04  | update code 
#}


{% set column_names = mcr_get_bussiness_columns(this) %}
--- config job
{{ 
    config(
        materialized='incremental',
        unique_key=['CST_ID', 'CL_X_CST_TP_ID' , 'tf_created_at'],
        pre_hook = scd2_revert('atomic', this),
        incremental_strategy='delete+insert'
    ) 
}}

--- declare all model in use
with intr as (

    select s.*
    from {{ ref('intr_awm_cl_x_cst__customer_segment') }}  s where TF_CREATED_AT = '{{ var("etl_date") }}'

)
, intr_row as (
  
    select  a.*
           ,{{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
           ,'{{ var("end_date") }}'::date as tf_updated_at
           ,{{dbt_date.today()}} ppn_dt
           ,{{dbt_date.now()}}::time ppn_tm
     from intr a
)

--- process if is daily run
{% if is_incremental() %}
    , destination_rows as (---lay cac ban ghi co hieu luc o thoi diem hien tai    
        select *
               , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
         from {{ this }} 
        where tf_updated_at = cast('{{ var("end_date") }}' as date)
    
    )

    , new_valid_to as (---xac dinh thong tin ban ghi moi duoc insert vao    
        select {{'s.'~column_names| join(',s.')}}
            , s.tf_created_at 
            , s.tf_updated_at::date as tf_updated_at ---end date
            , s.ppn_dt
            , s.ppn_tm::time  -- thong tin la thong tin moi nhat tu Source, hieu luc tu ngay Etl -> oo                         
          from intr_row s
          left join destination_rows d
            on s.CST_ID = d.CST_ID and s.CL_X_CST_TP_ID = d.CL_X_CST_TP_ID 
         where nvl(s.compare_key, '$')  != nvl(d.compare_key, '$')      
    )

    , add_new_valid_to as (---End date ban ghi cu
        select  {{'d.'~column_names| join(',d.')}}
                , d.tf_created_at 
                , '{{ var("etl_date") }}'::date as tf_updated_at  ---end date
                , n.ppn_dt
                , n.ppn_tm::time
          from destination_rows d
          join new_valid_to n
            on d.CST_ID = n.CST_ID and  d.CL_X_CST_TP_ID = n.CL_X_CST_TP_ID 

    )
    , not_exists_in_src as (  
        select  {{'d.'~column_names| join(',d.')}}
                ----------------
                , d.tf_created_at 
                , '{{ var("etl_date") }}'::timestamp as tf_updated_at  ---end date   
                ,{{dbt_date.today()}} ppn_dt
                ,{{dbt_date.now()}}::time ppn_tm
          from destination_rows d
          left join intr_row n
            on d.CST_ID = n.CST_ID 
            and d.CL_X_CST_TP_ID = n.CL_X_CST_TP_ID 
          where n.CST_ID is null  and n.CL_X_CST_TP_ID is null
    )
    , final as (
        select n.*
        from add_new_valid_to n
        union all
        select o.* 
        from new_valid_to o
        union all
        select d.* 
        from not_exists_in_src d
    )
{% endif %}

--- final data
select   
        
        {{'d.'~column_names| join(',d.')}}
        -----------------------------
        , d.tf_created_at 
        , d.tf_updated_at  
        , d.ppn_dt
        , d.ppn_tm::time
        ,'{{ model.path }}' job_nm
  from intr_row d
--- process if is daily run
{% if is_incremental() %}
    Where 1=2 
    union all 
    Select 
    {{'s.'~column_names| join(',s.')}} 
    -------------------------------------
    , s.tf_created_at 
    , s.tf_updated_at  
    , s.ppn_dt
    , s.ppn_tm
    ,'{{ model.path }}' job_nm
    from final s
    
{% endif %}
