{#
    job name : card_ar
    mapping ID : EAD_RA CARD_AR
    created by : ly.trinh
    modified by | date modify | description
    nam.tran    | 2023-09-04  | update column card_ar_nm
#}

{% set column_names = mcr_get_bussiness_columns(this) %}
--- config job
{{
    config(
        materialized = 'incremental',
        unique_key = 'card_ar_id',
        tags=['awm','card_ar']
    )
}}

--- declare all model in use
with way4_card_ar as (
    select a.*
    from {{ ref('intr_awm_card_ar__way4_card') }} a 
    where tf_created_at = '{{ var("etl_date") }}'::date
)
, final as (
    select 
    a.*
    , {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key
    , '{{ var("end_date") }}'::date as tf_updated_at
     from way4_card_ar a
)
--- process if is daily run
{% if is_incremental() %}
    ,trgt as(
        select  {{ dbt_utils.generate_surrogate_key( column_names )}} compare_key , t.*
        from {{this}} t
)
{% endif %}

--- final data
select 
    {{'s.'~column_names| join(',s.')}}
    --------------------------
    ,'{{ model.path }}' job_nm
    , s.tf_created_at 
    , s.tf_updated_at    
    ,{{dbt_date.today()}} ppn_dt
    ,{{dbt_date.now()}}::time ppn_tm 
    , null::date tf_deleted_at
from final s
--- process if is daily run
{% if is_incremental() %}
   left join trgt t on s.card_ar_id = t.card_ar_id
   where nvl(t.compare_key,'$') <> nvl(s.compare_key,'$')
   union all 
   select {{'t.'~column_names| join(',t.')}}
        , t.job_nm
        , t.tf_created_at 
        , t.tf_updated_at    
        , {{dbt_date.today()}} ppn_dt
        , {{dbt_date.now()}}::time ppn_tm 
        , '{{ var("etl_date") }}'::date as tf_deleted_at
   from trgt t 
   left join final s on s.card_ar_id = t.card_ar_id
   where s.card_ar_id is null
{% endif %}