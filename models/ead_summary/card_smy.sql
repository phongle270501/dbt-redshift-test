{#
    job name : card_smy
    mapping ID : EAD_Redshift CARD_SMY
    created by : tien.cm  
    modified by | date modify | description
    tien.cm     | 2023-09-04  | update code
#}

--- config job
{{
    config(
        materialized = 'incremental',
        pre_hook = "delete from {{this}} where SNPST_DT = REPLACE('{{ var('etl_date') }}', '-', '')::INT",
        tags=['smy','card_smy']
    )
}}
--- final data
select 
    s.PYMTC_ID
    ,s.SNPST_DT
    ,s.CARD_AR_ID
    ,s.PYMTC_CODE
    ,s.CARD_AR_CODE
    ,s.LMT_AMT
    ,s.OPN_BR_CODE
    ,s.OPN_BR_ID
    ,s.UNQ_ID_IN_SRC_STM
    ,s.LAST_ACTVN_DT
    ,s.CST_ID
    ,s.CST_CODE
    ,s.EFF_FM_DT
    ,s.EFF_TO_DT
    ,s.SRC_STM
    ,{{dbt_date.today()}} ppn_dt
    ,{{dbt_date.now()}}::time ppn_tm
    ,'{{ model.path }}' job_nm
from {{ ref('intr_awm_card_smy__card_ar') }} s
