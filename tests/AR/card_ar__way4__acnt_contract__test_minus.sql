{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_ar','test_awm'])}}

with 
f_i as
    (
        select 
            fi.id,
            case 
                when fi.bank_code = '0100' then 'VN0010001'
                else 'VN001'|| fi.bank_code
            end bank_code
        from {{ ref('stg_way4__f_i') }} fi
    )
,stg as 
    (
        select 
            {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.id'} , {'text': 'WAY4_ACNT_CONTRACT'}]) }} as card_ar_id,
            acnt_ctr.contract_number::varchar as card_ar_nm,
            acnt_ctr.date_open::date as eff_dt,
            acnt_ctr.date_expire::date as end_dt,
            {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ACNT_CONTRACT__OID'} , {'text': 'WAY4_ACNT_CONTRACT'}]) }} as prn_ar_id,
            {{ mcr_generate_surrogate_key([{'text': 'W4-ACC-CTR-ST'} , {'column':'acnt_ctr.CONTR_STATUS'} ,{'text': 'CSV_CV'}]) }} as card_lcs_tp_id,
            acnt_ctr.id::varchar as unq_id_in_src_stm,
            case 
                when acnt_ctr.CON_CAT = 'A' AND acnt_ctr.PCAT = 'M' then {{ mcr_generate_surrogate_key([{'text': 'AR_TP'} , {'text': 'W4-DVC-MRCH-CTR'}, {'text': 'CSV_CV'}]) }}
                when acnt_ctr.CON_CAT = 'A' AND acnt_ctr.PCAT = 'C' then {{ mcr_generate_surrogate_key([{'text': 'AR_TP'} , {'text': 'W4-ACC-CTR'}, {'text': 'CSV_CV'}]) }}
                when acnt_ctr.CON_CAT = 'C' then  {{ mcr_generate_surrogate_key([{'text': 'AR_TP'} , {'text': 'W4-CARD-CTR'}, {'text': 'CSV_CV'} ]) }} 
                else 
                {{ mcr_generate_surrogate_key([{'text': 'AR_TP'} , {'text': 'W4-OTHER-CTR'}, {'text': 'CSV_CV'} ]) }} 
            end ar_tp_id,
            {{ mcr_generate_surrogate_key([{'column': 'fi.bank_code'}, {'text': 'T24_VPB_COMPANY'} ]) }} opn_br_id,
            REGEXP_SUBSTR(acnt_ctr.ADD_INFO_04,'(AL(\d)+)', 1, 1) as dsc,
            {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.CLIENT__ID'}, {'text': 'WAY4_CLIENT'} ]) }} as cst_id
        from {{ ref('stg_way4__acnt_contract') }} acnt_ctr
        left join f_i fi on acnt_ctr.f_i = fi.id
        where acnt_ctr.base_relation is null
    ),
awm as
    (
        select 
            card_ar_id,
            card_ar_nm,
            eff_dt,
            end_dt,
            prn_ar_id,
            card_lcs_tp_id,
            unq_id_in_src_stm,
            ar_tp_id,
            opn_br_id,
            dsc,
            cst_id
        from {{ ref('intr_awm_card_ar__way4_card') }}
        where SRC_STM_ID = {{ mcr_generate_surrogate_key([{'text': 'WAY4_ACNT_CONTRACT'} , {'text': 'CSV_SRC_STM'}]) }}
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )
select * from check_minus