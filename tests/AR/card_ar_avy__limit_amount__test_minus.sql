{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_ar','test_awm'])}}

with stg as 
    (
        select
            {{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ID'}, {'text': 'WAY4_ACNT_CONTRACT'} ]) }} as card_ar_id
            ,{{ mcr_generate_surrogate_key([{'column': 'acnt_ctr.ID'}, {'text': 'W4-CARD-CTR-LMT-AMT'}, {'text': 'WAY4_ACNT_CONTRACT'} ]) }} as card_ar_avy_id
            ,acnt_ctr.account_limit as avy_amt
            ,{{ mcr_generate_surrogate_key([{'text': 'AR-AVY-TP'}, {'text': 'W4-CARD-CTR-LMT-AMT'}, {'text': 'CSV_CV'} ]) }} as avy_tp_id
        from {{ ref('stg_way4__tbl_w4_acnt_contract') }} acnt_ctr
        where acnt_ctr.con_cat = 'C'
            and acnt_ctr.account_limit is not null
    ),
awm as
    (
        select
            card_ar_id
            ,card_ar_avy_id
            ,avy_amt
            ,avy_tp_id
        from {{ ref('card_ar_avy') }}
        where avy_tp_id = {{ mcr_generate_surrogate_key([{'text': 'AR-AVY-TP'} , {'text': 'W4-CARD-CTR-LMT-AMT'}, {'text': 'CSV_CV'}]) }}
            and tf_created_at <= '{{var("etl_date") }}'::DATE
            and tf_updated_at > '{{var("etl_date") }}'::DATE
    ),
check_minus as
    (
        select 'stg'::varchar as source, stg.* from stg
            minus
        select 'stg'::varchar as source, awm.* from awm

        union all
        
        select 'awm'::varchar as source, awm.* from awm
            minus
        select 'awm'::varchar as source, stg.* from stg
    )
select * from check_minus