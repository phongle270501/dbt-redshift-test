{{ config(store_failures = true,
            schema = 'dbt_test__audit',
            pre_hook = 'drop table {{this}}',
            tags = ['test_ar','test_awm'])}}

with count_stg as 
    (
        select 'stg_way4__acnt_contract'::varchar as source, count(*) as count_row
        from {{ ref('stg_way4__acnt_contract') }} acnt_ctr
            left join {{ ref('stg_way4__vpb_acnt_pd_date') }} pd_date on acnt_ctr.id = pd_date.ACNT_CONTRACT__OID
        where acnt_ctr.con_cat = 'C'
            and pd_date.pd_date is not null
    ),
count_awm as
    (
        select 'card_ar_avy'::varchar as source, count (*) as count_row
        from {{ ref('card_ar_avy') }}
        where avy_tp_id = {{ mcr_generate_surrogate_key([{'text': 'AR-AVY-TP'} , {'text': 'W4-ACC-CTR-ODUE-DT'}, {'text': 'CSV_CV'}]) }}
            and tf_created_at <= '{{var("etl_date") }}'::DATE
            and tf_updated_at > '{{var("etl_date") }}'::DATE
    ),
check_count as
    (
        select
            {{dbt_date.today()}} ppn_dt,
            'Test_count'::varchar as Test_type,
            a.source as source_tbl,
            b.source as target_tbl,
            a.count_row as count_source,
            b.count_row as count_target
        from count_stg a 
        full join count_awm b on 1=1
    )
    select
        *
    from check_count where count_source <> count_target
        union
    select
        *
    from check_count where count_target = 0